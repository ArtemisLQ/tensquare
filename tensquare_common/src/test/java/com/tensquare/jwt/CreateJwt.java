package com.tensquare.jwt;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/30 10:29
 * @Modified By:
 */
public class CreateJwt {
    public static void main(String[] args) {
        JwtBuilder builder = Jwts.builder()
                .setId("1")
                .setSubject("hijwt")
//                签发时间
                .setIssuedAt(new Date())
//                签名密钥
                .signWith(SignatureAlgorithm.HS256,"liliang")
                .claim("role","admin")
                .setExpiration(new Date(new Date().getTime() + 60000));
        System.out.println(builder.compact());
    }
}
