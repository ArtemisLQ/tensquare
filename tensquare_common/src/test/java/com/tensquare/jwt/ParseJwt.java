package com.tensquare.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.text.SimpleDateFormat;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/30 10:42
 * @Modified By:
 */
public class ParseJwt {
    public static void main(String[] args) {

        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxIiwic3ViIjoiaGlqd3QiLCJpYXQiOjE1NzI0MDU3NjIsInJvbGUiOiJhZG1pbiIsImV4cCI6MTU3MjQwNTgyMn0.4G1lcDRZc3MERYFcldc_lsdUXlToQBNyZ8HoZMzua4o";
        Claims claims = Jwts.parser().setSigningKey("liliang").parseClaimsJws(token).getBody();
        System.out.println("id:" + claims.getId());
        System.out.println("subject:" + claims.getSubject());
        System.out.println("签发时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(claims.getIssuedAt()));
        System.out.println("过期时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(claims.getExpiration()));
        System.out.println("角色:" + claims.get("role"));
    }
}
