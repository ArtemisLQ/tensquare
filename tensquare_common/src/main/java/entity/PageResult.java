package entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author: LL
 * @Description:    分页结果类
 * @Date: Created in 2019/10/20 17:46
 * @Modified By:
 */
@Getter
@Setter
public class PageResult<T> {
    private Long total;
    private List<T> rows;

    public PageResult(Long total, List<T> rows) {
        super();
        this.total = total;
        this.rows = rows;
    }
}
