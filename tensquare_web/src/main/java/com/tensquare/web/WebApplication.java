package com.tensquare.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/11/6 22:18
 * @Modified By:
 */
@EnableZuulProxy
@SpringBootApplication
@EnableEurekaClient//在导入jar包和写入application配置文件之后可以省略此注解
public class WebApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}
