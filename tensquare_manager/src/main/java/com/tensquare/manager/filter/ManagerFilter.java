package com.tensquare.manager.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import util.JwtUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/11/6 22:22
 * @Modified By:
 */
@Component
public class ManagerFilter extends ZuulFilter {

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public String filterType() {
        return "pre";// 前置过滤器
    }

    @Override
    public int filterOrder() {
        return 0;// 优先级为0，数字越大，优先级越低
    }

    @Override
    public boolean shouldFilter() {
        return true;// 是否执行该过滤器，此处为true，说明需要过滤
    }

    @Override
    public Object run() {
        System.out.println("经过了zuul过滤器...");
        //获取上下文
        RequestContext requestContext = RequestContext.getCurrentContext();
        //获取请求域
        HttpServletRequest request = requestContext.getRequest();
        //请求zuul网关其实会请求两次，第一次是分配路由，第二次是正式请求。所以当第一请求发起的时候需要直接通过过滤器。
        if (request.getMethod().equals("OPTIONS")) {
            return null;
        }

        //登录请求除外
        String url = request.getRequestURL().toString();
        if (url.indexOf("/admin/login") > 0) {
            System.out.println("登陆页面" + url);
            return null;
        }


        //获取头信息
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null && !"".equals(authHeader) && authHeader.startsWith("Bearer ")) {
            String token = authHeader.substring(7);
            try {
                //解析验证token
                Claims claims = jwtUtil.parseJWT(token);
                //判断为管理员角色的时候，才转发token
                if ("admin".equals(claims.get("roles"))) {
                    requestContext.addZuulRequestHeader("Authorization", authHeader);
                    System.out.println("token 验证通过，添加了头信息" + authHeader);
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                requestContext.setSendZuulResponse(false);//终止运行
            }

        }
        requestContext.setSendZuulResponse(false);//终止运行
        requestContext.setResponseStatusCode(403);
        requestContext.setResponseBody("无权访问");
        requestContext.getResponse().setContentType("text/html;charset=UTF-8");
        return null;

    }
}
