package com.tensquare.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import util.IdWorker;


/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/24 22:26
 * @Modified By:
 */
@SpringBootApplication
public class SearchApplicaion {
    public static void main(String[] args) {
        SpringApplication.run(SearchApplicaion.class, args);
    }

    @Bean
    public IdWorker idWorkker() {
        return new IdWorker(1, 1);
    }
}
