package com.tensquare.search.pojo;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.io.Serializable;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/24 22:29
 * @Modified By:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(indexName = "tensquare", type = "article")
public class Article implements Serializable {
    @Id
    private String id;//ID

    //是否索引，就是看该域是否能被搜索
    //是否分词，搜索的时候是整体匹配还是单词匹配
    //是否存储，是否在页面上显示，写在这个pojo类中就表示是被存储的
    @Field(index = true, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String title;//标题

    @Field(index = true, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String content;//文章正文

//    private String state;//审核状态

}
