package com.tensquare.search.service;

import com.tensquare.search.dao.ArticleSearchDao;
import com.tensquare.search.pojo.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/25 12:07
 * @Modified By:
 */
@Service
public class ArticleSearchService {
    @Autowired
    private ArticleSearchDao articleSearchDao;

    /**
     * @param article
     * @Author: LL
     * @Return: void
     * @Description: 增加文章进索引库
     */
    public void add(Article article) {
        articleSearchDao.save(article);
    }

    /**
     * @param keywords
     * @param page
     * @param size
     * @Author: LL
     * @Return: org.springframework.data.domain.Page<com.tensquare.search.pojo.Article>
     * @Description: 文章搜索
     */
    public Page<Article> search(String keywords, int page, int size) {
        return articleSearchDao.findByTitleOrContentLike(keywords, keywords, PageRequest.of(page, size));
    }
}
