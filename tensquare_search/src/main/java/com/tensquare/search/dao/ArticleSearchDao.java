package com.tensquare.search.dao;

import com.tensquare.search.pojo.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/25 12:02
 * @Modified By:
 */
public interface ArticleSearchDao extends ElasticsearchRepository<Article, String> {
    /**
     * @Author: LL
     * @param   title
     * @param   content
     * @param   pageable
     * @Return: org.springframework.data.domain.Page<com.tensquare.search.pojo.Article>
     * @Description: 文章搜索
     */
    Page<Article> findByTitleOrContentLike(String title, String content, Pageable pageable);
}
