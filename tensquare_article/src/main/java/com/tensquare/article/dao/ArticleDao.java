package com.tensquare.article.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.article.pojo.Article;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface ArticleDao extends JpaRepository<Article, String>, JpaSpecificationExecutor<Article> {

    /**
     * @param id
     * @Author: LL
     * @Return: void
     * @Description: 文章审核
     */
    @Modifying
    @Query(value = "update tb_article set state=1 where id = ?", nativeQuery = true)
    void updateState(String id);

    /**
     * @Author: LL
     * @param   id
     * @Return: void
     * @Description: 文章点赞
     */
    @Modifying
    @Query(value = "update tb_article set thumbup = thumbup+1 where id= ?", nativeQuery = true)
    void updateThumbup(String id);

}
