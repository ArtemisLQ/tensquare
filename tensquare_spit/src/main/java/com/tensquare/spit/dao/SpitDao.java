package com.tensquare.spit.dao;

import com.tensquare.spit.pojo.Spit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/23 14:49
 * @Modified By:
 */
public interface SpitDao extends MongoRepository<Spit, String> {
    Page<Spit> findByParentid(String id, Pageable pageable);
}
