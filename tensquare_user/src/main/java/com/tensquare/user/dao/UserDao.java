package com.tensquare.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.user.pojo.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface UserDao extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

    /**
     * @param mobile
     * @Author: LL
     * @Return: com.tensquare.user.pojo.User
     * @Description: 根据手机号查询
     */
    User findByMobile(String mobile);

    /**
     * 更新粉丝数
     *
     * @param userid 用户ID
     * @param x      粉丝数
     */
    @Modifying
    @Query(value = "update tb_user set fanscount= fanscount+? where id=?", nativeQuery = true)
    void incFanscount(int x, String userid);

    /**
     * 更新关注数
     *
     * @param userid 用户ID
     * @param x      粉丝数
     */
    @Modifying
    @Query(value = "update tb_user set followcount= followcount+? where id=?", nativeQuery = true)
    void incFollowcount(int x, String userid);

}
