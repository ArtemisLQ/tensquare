package com.tensquare.rabbitmq.customer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/28 14:22
 * @Modified By:
 */
@Component
@RabbitListener(queues = "Demo3")
public class Customer3 {
    @RabbitHandler
    public void showMessage(String message) {
        System.out.println("Demo3接收到消息：" + message);
    }
}
