package com.tensquare.rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/28 14:12
 * @Modified By:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RabbitmqApplication.class)
public class MqTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend() {
        rabbitTemplate.convertAndSend("Demo", "直接模式测试");
    }

    @Test
    public void testSendFanout() {
        rabbitTemplate.convertAndSend("amp.fanout", "", "分裂模式测试");
    }

    @Test
    public void testSendTopic() {
        rabbitTemplate.convertAndSend("amq.topic", "good.abc.log", "主题模式测试");
    }
}

