package com.tensquare.friend.dao;

import com.tensquare.friend.pojo.NoFriend;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/11/6 15:37
 * @Modified By:
 */
public interface NoFriendDao extends JpaRepository<NoFriend,String> {
}
