package com.tensquare.friend.dao;

import com.tensquare.friend.pojo.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/11/5 22:39
 * @Modified By:
 */
public interface FriendDao extends JpaRepository<Friend, String>, JpaSpecificationExecutor<Friend> {

    Friend findByUseridAndFriendid(String userid, String friendid);

    /**
     * 根据用户ID与被关注用户ID查询记录个数
     *
     * @param userid
     * @param friendid
     * @return
     */
    @Query(value = "select count(*) from tb_friend where userid= ? and friendid = ?", nativeQuery = true)
    int selectCount(String userid, String friendid);

    /**
     * 更新为互相喜欢
     *
     * @param userid
     * @param friendid
     */
    @Modifying
    @Query(value = "update tb_friend set islike = ? where userid= ? and friendid = ?", nativeQuery = true)
    void updateLike(String userid, String friendid, String islike);

    /**
     * 删除喜欢
     *
     * @param userid
     * @param friendid
     */
    @Modifying
    @Query(value = "delete from tb_friend where userid=? and friendid=?", nativeQuery = true)
    void deleteFriend(String userid, String friendid);


}
