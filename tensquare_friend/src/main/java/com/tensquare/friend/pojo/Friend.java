package com.tensquare.friend.pojo;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/11/5 22:33
 * @Modified By:
 */
@Entity
@Table(name = "tb_friend")
@IdClass(Friend.class)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Friend implements Serializable {
    @Id
    private String userid;

    @Id
    private String friendid;

    private String islike;
}
