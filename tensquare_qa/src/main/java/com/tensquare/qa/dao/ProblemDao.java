package com.tensquare.qa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.qa.pojo.Problem;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface ProblemDao extends JpaRepository<Problem, String>, JpaSpecificationExecutor<Problem> {

    /**
     * @param labelId
     * @param pageable
     * @Author: LL
     * @Return: org.springframework.data.domain.Page<com.tensquare.qa.pojo.Problem>
     * @Description: 最新问答列表
     */
    @Query(value = "select * from tb_problem tp,tb_pl tl where tl.problemid=tp.id and tl.labelid = ? order by tp.replytime desc", nativeQuery = true)
    Page<Problem> findNewListByLabelId(String labelId, Pageable pageable);

    /**
     * @param labelId
     * @param pageable
     * @Author: LL
     * @Return: org.springframework.data.domain.Page<com.tensquare.qa.pojo.Problem>
     * @Description: 热门问答列表
     */
    @Query(value = "select * from tb_problem tp,tb_pl tl where tl.problemid=tp.id and tl.labelid = ? order by tp.reply desc", nativeQuery = true)
    Page<Problem> findHotListByLabelId(String labelId, Pageable pageable);

    /**
     * @param labelId
     * @param pageable
     * @Author: LL
     * @Return: org.springframework.data.domain.Page<com.tensquare.qa.pojo.Problem>
     * @Description: 等待回答列表
     */
    @Query(value = "select * from tb_problem tp,tb_pl tl where tl.problemid=tp.id and tl.labelid = ? and tp.reply=0 order by tp.createtime desc", nativeQuery = true)
    Page<Problem> findWaitListByLabelId(String labelId, Pageable pageable);

}
