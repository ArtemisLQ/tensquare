package com.tensquare.qa.com.tensquare.qa.client.fallback;

import com.tensquare.qa.com.tensquare.qa.client.BaseClient;
import entity.Result;
import entity.StatusCode;
import org.springframework.stereotype.Component;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/11/6 21:25
 * @Modified By:
 */
@Component
public class BaseClientFallback implements BaseClient {
    @Override
    public Result findById(String id) {
        return new Result(false, StatusCode.ERROR,"熔断器启动了");
    }
}
