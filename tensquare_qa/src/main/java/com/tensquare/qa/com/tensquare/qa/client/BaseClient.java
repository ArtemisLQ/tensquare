package com.tensquare.qa.com.tensquare.qa.client;

import com.tensquare.qa.com.tensquare.qa.client.fallback.BaseClientFallback;
import entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: LL
 * @Description:
 * @Date: Created in 2019/10/31 14:13
 * @Modified By:
 */

@FeignClient(value = "tensquare-base", fallback = BaseClientFallback.class)
public interface BaseClient {
    @RequestMapping(value = "/label/{id}", method = RequestMethod.GET)
    Result findById(@PathVariable String id);
}
